# General Makefile settings

## Purpose

These are various makefile templates.  They will require some settings to be
changed in order to work correctly -- namely the SRCS variable, if not more.

Each makefile has instructions, at the top, for ease of use.  Please refer to
each file for directions.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/makefile.git

## Placement

Place each `Makefile` in the working directory of your program, and modify the
appropriate variables.  As long as you modify the variables correctly, each
makefile should work correctly, with the appropriate functions.

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/makefiles/src/1233d34ce29cc75d485e064e76ffb3b99368c520/LICENSE.txt?at=master) file for
details.

